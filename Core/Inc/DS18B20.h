#ifndef __DS18B20_H_
#define __DS18B20_H_

#include "stdint.h"
#include "stdio.h"
#include "string.h"

#include "main.h"
#include "gpio.h"

typedef enum
{
    I_Have_ROM,
    Skip
} DS18B20_ModeTypedef;

typedef enum
{
    ConvertT = 0x44,
    ReadScrachpad = 0xBE,
    WriteScrachpad = 0x4E,
    CopyScrachpad = 0x48,
    RecallE2ROM = 0xB8,
    ReadPowerSupply = 0xB4
} DS18B20FUNCCMDTypdef;

typedef enum
{
    SearchRom = 0xF0,
    ReadRom = 0x33,
    MatchRom = 0x55,
    SkipRom = 0xCC,
    AlarmSerch = 0xEC,
} DS18B20ROMCMDTypdef;

typedef struct
{
    struct
    {
        void (*Config)(DS18B20_ModeTypedef mode);
        float (*ReadTemprature)(uint64_t ROM);
        void (*SearchForROM)();
        uint64_t (*ReadROM)();
    } Func;

    union
    {
        struct
        {
            uint8_t TL;
            uint8_t TH;
            uint8_t TL_USER;
            uint8_t TH_USER;
            uint8_t ConfigR;
            uint8_t reserved0;
            uint8_t reserved1;
            uint8_t Reserved2;
            uint8_t CRC_Val;
        } u8type;
        uint64_t u64type;
    } Data;

    uint64_t Num;
    DS18B20_ModeTypedef mode;
} DS18B20;

static void Config(DS18B20_ModeTypedef mode);

extern DS18B20 DS18B20_Ob;
#endif // !__DS