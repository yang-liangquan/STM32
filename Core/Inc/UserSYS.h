#ifndef __USERSYS_H__
#define __USERSYS_H__

#include "stdint.h"
#include "stdio.h"
#include "string.h"

#include "main.h"
#include "tim.h"
#include "usart.h"

#define PRINT_MAX_SIZE 100

#define PRINT(...)                                                                         \
    {                                                                                      \
        while (__HAL_UART_GET_FLAG(&huart1, UART_FLAG_TC) == 0)                            \
            ;                                                                              \
        char char_print[PRINT_MAX_SIZE] = {0};                                             \
        sprintf(char_print, __VA_ARGS__);                                                  \
        HAL_UART_Transmit(&huart1, (const uint8_t *)char_print, strlen(char_print), 0xff); \
    }

void Delay_Us(uint16_t us);

#endif // !__USERSYS_H