/*
 * DS18B20.c
 *
 *  Created on: Oct 24, 2023
 *      Author: yangl
 */

#include "DS18B20.h"

DS18B20 DS18B20_Ob = {{Config}, {0}};

static GPIO_InitTypeDef GPIO_InitStructure = {DS18B20_PIN_Pin, 0, 0, GPIO_SPEED_FAST, 0};

/**
 * The function initializes a DS18B20 temperature sensor by configuring GPIO pins and checking for a
 * response from the sensor.
 *
 * @return a uint8_t value, which is an unsigned 8-bit integer. The function returns 1 if the
 * DS18B20_PIN_GPIO_Port does not respond, indicating a failed initialization. Otherwise, it returns 0
 * after a successful initialization.
 */
static uint8_t Init(void)
{
    GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(DS18B20_PIN_GPIO_Port, &GPIO_InitStructure);

    DS18B20_PIN_GPIO_Port->BSRR = (DS18B20_PIN_Pin << 16);
    Delay_Us(500);
    DS18B20_PIN_GPIO_Port->BSRR = (DS18B20_PIN_Pin);
    Delay_Us(30);
    GPIO_InitStructure.Mode = GPIO_MODE_INPUT;
    GPIO_InitStructure.Pull = GPIO_PULLUP;
    HAL_GPIO_Init(DS18B20_PIN_GPIO_Port, &GPIO_InitStructure);

    if (DS18B20_PIN_GPIO_Port->IDR & DS18B20_PIN_Pin)
    {
        PRINT("Initialize DS18B20 failed!! --No Response...\r\n");
        return 1;
    }
    else
    {
        while (!(DS18B20_PIN_GPIO_Port->IDR & DS18B20_PIN_Pin))
            ;
        Delay_Us(400);
        return 0;
    }
}

/**
 * The function SendByte sends a byte of data using a specific protocol.
 *
 * @param byte The parameter "byte" is of type uint8_t, which means it is an unsigned 8-bit integer. It
 * represents the byte of data that needs to be sent.
 */
static void SendByte(uint8_t byte)
{
    GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(DS18B20_PIN_GPIO_Port, &GPIO_InitStructure);

    for (uint8_t i = 0; i < 8; i++)
    {
        DS18B20_PIN_GPIO_Port->BSRR = (DS18B20_PIN_Pin << 16);
        byte & 0x01 ? Delay_Us(5) : Delay_Us(72);
        DS18B20_PIN_GPIO_Port->BSRR = (DS18B20_PIN_Pin);
        byte & 0x01 ? Delay_Us(60) : Delay_Us(5);
        byte >>= 1;
    }
}

/**
 * The function ReadByte reads a byte of data from a DS18B20 temperature sensor using a specific
 * protocol.
 *
 * @return a uint8_t (unsigned 8-bit integer) value, which represents a byte of data.
 */
static uint8_t ReadByte(void)
{
    uint8_t byte = 0;
    for (uint8_t i = 0; i < 8; i++)
    {
        GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
        GPIO_InitStructure.Pull = GPIO_NOPULL;
        HAL_GPIO_Init(DS18B20_PIN_GPIO_Port, &GPIO_InitStructure);

        DS18B20_PIN_GPIO_Port->BSRR = (DS18B20_PIN_Pin << 16);
        Delay_Us(3);

        GPIO_InitStructure.Mode = GPIO_MODE_INPUT;
        GPIO_InitStructure.Pull = GPIO_PULLUP;
        HAL_GPIO_Init(DS18B20_PIN_GPIO_Port, &GPIO_InitStructure);

        byte >>= 1;
        Delay_Us(5);

        if (DS18B20_PIN_GPIO_Port->IDR & DS18B20_PIN_Pin)
        {
            byte |= 0x80;
        }
        Delay_Us(90);
    }
    GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(DS18B20_PIN_GPIO_Port, &GPIO_InitStructure);

    DS18B20_PIN_GPIO_Port->BSRR = DS18B20_PIN_Pin;
    return byte;
}

/**
 * The function "readScrachpad" reads 9 bytes of data and stores them in a structure.
 */
static void readScrachpad()
{

    for (uint8_t i = 0; i < 9; i++)
    {
        *((uint8_t *)(&DS18B20_Ob.Data.u8type.TL) + i) = ReadByte();
    }
}

/**
 * The function `ROM_Analyze` analyzes data from a ROM and performs certain operations based on the
 * data.
 */
static void ROM_Analyze()
{
    uint64_t Data[2] = {0};
    uint64_t ROM = 0;
    for (uint8_t j = 0; j < 2; j++)
    {
        for (uint8_t i = 0; i < 64; i++)
        {
            GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
            GPIO_InitStructure.Pull = GPIO_NOPULL;
            HAL_GPIO_Init(DS18B20_PIN_GPIO_Port, &GPIO_InitStructure);

            DS18B20_PIN_GPIO_Port->BSRR = (DS18B20_PIN_Pin << 16);
            Delay_Us(1);

            GPIO_InitStructure.Mode = GPIO_MODE_INPUT;
            GPIO_InitStructure.Pull = GPIO_PULLUP;
            HAL_GPIO_Init(DS18B20_PIN_GPIO_Port, &GPIO_InitStructure);

            Data[j] >>= 1;
            Delay_Us(5);

            if (DS18B20_PIN_GPIO_Port->IDR & DS18B20_PIN_Pin)
            {
                Data[j] |= 0x80;
            }
            Delay_Us(60);
        }
    }
    for (uint8_t y = 0; y < 2; y++)
    {
        for (uint8_t z = 0; z < 64; z++)
        {
            ROM >>= 1;
            if (Data[y] >> 0x01)
            {
                ROM |= 0x80;
            }
            else if (Data[y] >> 0x02)
            {
                ROM &= 0x00;
            }
            else if (Data[y] == 0)
            {
                DS18B20_Ob.Num += 1;
            }
        }
    }
}

/**
 * The function "sendROMCMD" sends a command to a DS18B20 device using its ROM code.
 *
 * @param cmd The parameter "cmd" is of type DS18B20ROMCMDTypdef, which is a user-defined data type. It
 * likely represents a command to be sent to a DS18B20 temperature sensor.
 *
 * @return nothing (void).
 */
static void sendROMCMD(DS18B20ROMCMDTypdef cmd)
{
    if (Init())
        return;
    SendByte(cmd);
}

/**
 * The function sendFUNCCMD sends a command to a DS18B20 device.
 *
 * @param cmd The parameter "cmd" is of type DS18B20FUNCCMDTypdef, which is a user-defined data type.
 * It is likely an enumeration or a structure that represents different commands for the DS18B20
 * temperature sensor.
 */
static void sendFUNCCMD(DS18B20FUNCCMDTypdef cmd)
{
    SendByte(cmd);
}

/**
 * The function "sendROM" sends a 64-bit value in 8 bytes by repeatedly sending each byte in
 * little-endian order.
 *
 * @param ROM The parameter "ROM" is of type uint64_t, which is an unsigned 64-bit integer. It is used
 * to represent a value that is 8 bytes long.
 */
static void sendROM(uint64_t ROM)
{
    for (uint8_t i = 0; i < 8; i++)
    {
        SendByte(ROM & 0xff);
        ROM >>= 8;
    }
}
/**
 * The function "readROM" reads the ROM value from a DS18B20 temperature sensor.
 *
 * @return a 64-bit unsigned integer (uint64_t) value, which represents the ROM (Read-Only Memory) read
 * from a DS18B20 temperature sensor.
 */
static uint64_t readROM()
{
    uint64_t ROM = 0;
    sendROMCMD(ReadRom);
    for (uint8_t i = 0; i < 64; i++)
    {
        GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
        GPIO_InitStructure.Pull = GPIO_NOPULL;
        HAL_GPIO_Init(DS18B20_PIN_GPIO_Port, &GPIO_InitStructure);

        DS18B20_PIN_GPIO_Port->BSRR = (DS18B20_PIN_Pin << 16);
        Delay_Us(3);

        GPIO_InitStructure.Mode = GPIO_MODE_INPUT;
        GPIO_InitStructure.Pull = GPIO_PULLUP;
        HAL_GPIO_Init(DS18B20_PIN_GPIO_Port, &GPIO_InitStructure);

        ROM >>= 1;
        Delay_Us(5);

        if (DS18B20_PIN_GPIO_Port->IDR & DS18B20_PIN_Pin)
        {
            ROM |= 0x8000000000000000;
        }
        Delay_Us(90);
    }
    GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(DS18B20_PIN_GPIO_Port, &GPIO_InitStructure);

    DS18B20_PIN_GPIO_Port->BSRR = DS18B20_PIN_Pin;
    PRINT("Current ROM:%#llx\r\n", ROM);
    return ROM;
}

/**
 * The function "readTemperatureSkip" reads the temperature from a DS18B20 sensor using the Skip ROM
 * command.
 *
 * @param ROM The parameter "ROM" is of type uint64_t and it represents the unique identifier of a
 * DS18B20 temperature sensor.
 *
 * @return a float value, which is the temperature read from the DS18B20 temperature sensor.
 */
static float readTemperature(uint64_t ROM)
{
    sendROMCMD(DS18B20_Ob.mode == I_Have_ROM ? MatchRom : SkipRom);
    sendROM(ROM);
    DS18B20_Ob.mode == I_Have_ROM ? sendROM(ROM) : __NOP();
    sendFUNCCMD(ConvertT);
    HAL_Delay(7);
    sendROMCMD(DS18B20_Ob.mode == I_Have_ROM ? MatchRom : SkipRom);
    DS18B20_Ob.mode == I_Have_ROM ? sendROM(ROM) : __NOP();
    sendFUNCCMD(ReadScrachpad);
    readScrachpad();
    return (float)(DS18B20_Ob.Data.u8type.TH << 8 | DS18B20_Ob.Data.u8type.TL) * 0.0625;
}

/**
 * The function searchForROM searches for ROM devices and prints the number of devices detected.
 */
static void searchForROM()
{
    sendROMCMD(SearchRom);
    Delay_Us(100);
    ROM_Analyze();
    PRINT("ROM_Analyze finished,%lld devices detected\r\n", DS18B20_Ob.Num + 1);
}

/**
 * The function "Config" sets the mode of the DS18B20 sensor and assigns appropriate functions based on
 * the mode.
 *
 * @param mode The "mode" parameter is of type DS18B20_ModeTypedef. It is used to configure the mode of
 * the DS18B20 sensor. The possible values for this parameter are "I_Have_ROM" and any other value.
 */
static void Config(DS18B20_ModeTypedef mode)
{
    DS18B20_Ob.mode = mode;
    DS18B20_Ob.Func.ReadTemprature = readTemperature;
    DS18B20_Ob.Func.SearchForROM = searchForROM;
    DS18B20_Ob.Func.ReadROM = readROM;
}