#include "UserSYS.h"

void Delay_Us(uint16_t us)
{
    __HAL_TIM_SET_COUNTER(&DELAY_TIM, 0);
    HAL_TIM_Base_Start(&DELAY_TIM);
    while (__HAL_TIM_GET_COUNTER(&DELAY_TIM) < us)
        ;
    HAL_TIM_Base_Stop(&DELAY_TIM);
}